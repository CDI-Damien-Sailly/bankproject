package command;

import interf.ICommand;
import object.Account;
import program.Bank;
import program.Programm;
import tools.Clavier;
import tools.Constant;

public class CommandCreateAccount implements ICommand {
	
	public static final String CMD ="1";
	private static final String DESC ="Cr�er un compte.";
	
	static {
		Programm.addCommandeDesc(CMD, DESC);
	}
	
	public static void chargerStaticPortion() {
		
	}

	@Override
	public void run() {
		System.out.println("Rentrez un num�ro de compte :");
		String nom = Clavier.readLine();
		String num = null;
		Account cpt;
		if(!nom.equals("")) {
			num = nom.toLowerCase();
		}else {
			System.out.println(Constant.WRONG_PARAMETER);
			return;
		}
		System.out.println("Rentrez le type du compte :");
		String acc = Clavier.readLine();
		String type = null;
		if(acc.equalsIgnoreCase("courant")||acc.equalsIgnoreCase("joint")||acc.equalsIgnoreCase("epargne")) {
			type = acc.toLowerCase();
			}else {
				System.out.println(Constant.WRONG_PARAMETER);}
		cpt = new Account(num,type);
		Bank.add(cpt, cpt.getNumCompt());
	}
	

}
