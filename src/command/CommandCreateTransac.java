package command;

import interf.ICommand;
import object.Transaction;
import tools.Clavier;
import tools.Constant;

public class CommandCreateTransac implements ICommand{

	@Override
	public void run() {
		
		String moyen = null;
		CommandCreateTransac.paidMode();
		if(CommandCreateTransac.paidMode().equals(Constant.WRONG_PAID)){
			System.out.println(CommandCreateTransac.paidMode());
			return;
		}else {
			moyen = CommandCreateTransac.paidMode();
		}
				
		Transaction	tra = new Transaction(moyen, CommandCreateTransac.montant(), CommandCreateTransac.theme());
		
		
	}
	
	public static String paidMode() {
		System.out.println("Saisir moyen de paiment (CB/Cheque/Virement : ");
		String moyen2 = Clavier.readLine();
		if(moyen2.equalsIgnoreCase("Cheque")||moyen2.equalsIgnoreCase("virement")) {
			return moyen2.toLowerCase();
			}else if (moyen2.equalsIgnoreCase("CB")){
				return moyen2.toUpperCase();
				}else {
					return Constant.WRONG_PAID;
		
				}
		}
	
	public static double montant() {
		System.out.println("Rentrez le montant de la transaction");
		return Clavier.readDouble();
	}
	
	public static String theme() {
		System.out.println("Rentrez le theme de la transaction");
		return Clavier.readLine();
	}
}