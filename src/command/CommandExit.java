package command;

import interf.ICommand;
import program.Programm;

public class CommandExit implements ICommand{

	public static final String CMD = "4";
	private static final String DESC = "Quitte le programme";
	
	static {
		Programm.addCommandeDesc(CMD, DESC);
	}

	public static void chargerStaticPortion() {
	}
	
	@Override
	public void run() {
		
	}

}
