package command;

import java.util.Map;
import java.util.Map.Entry;

import interf.ICommand;
import object.Account;
import program.Bank;
import program.Programm;
import tools.Clavier;

public class CommandPrintAccount implements ICommand{

	public static final String CMD ="2";
	private static final String DESC ="Afficher un compte.";
	
	static {
		Programm.addCommandeDesc(CMD, DESC);
	}
	
	public static void chargerStaticPortion() {
		
	}

	@Override
	public void run() {
		
		System.out.println("Rentrez le num�ro de compte � afficher : ");
		String num = Clavier.readLine();
		final Map<Account, String> accountList =Bank.getAccountlist();
		for(Entry<Account, String> e :accountList.entrySet()) {
			if(e.getKey().getNumCompt().equals(num)) {
				System.out.println("Compte num�ro : " + e.getKey());
			}
		}
		

	}
}
