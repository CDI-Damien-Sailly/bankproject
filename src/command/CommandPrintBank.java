package command;

import java.util.Map;
import java.util.Map.Entry;

import interf.ICommand;
import object.Account;
import program.Bank;
import program.Programm;

public class CommandPrintBank implements ICommand {
	
	public static final String CMD ="3";
	private static final String DESC ="Afficher tout les num�ros de compte";
	
	static {
		Programm.addCommandeDesc(CMD, DESC);
	}
	
	public static void chargerStaticPortion() {
		
	}

	@Override
	public void run() {
		
		final Map<Account, String> accountList =Bank.getAccountlist();
		for(Entry<Account, String> e :accountList.entrySet()) {
				System.out.println(e.getKey().getNumCompt());
		}
		

	}

}
