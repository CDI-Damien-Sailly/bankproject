package command;

import interf.ICommand;

public class CommandWrong implements ICommand{

	@Override
	public void run() {
		System.out.println("Command not found.");
	}

}
