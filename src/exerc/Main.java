package exerc;

import command.CommandExit;
import interf.ICommand;
import menu.Menu;
import program.Programm;
import tools.Clavier;

public class Main {
	
	public static void main (String[]agrs) {
		String cmdStr;
		ICommand command;
		do {
			Menu.menu1();
		cmdStr = Clavier.readLine();
		command = Programm.create(cmdStr);
		command.run();
		}while(!(command instanceof CommandExit));
	}

}