package interf;

public interface ICommand {
	
	void run();

}
