package object;

import java.util.ArrayList;

public class Account{
	
	private String numCompt;
	private String type;
	private double tauxPlacement = 2.0;
	private ArrayList<Transaction> transList;
	
	public Account(String numCompt, String type) {
		this.numCompt = numCompt;
		this.type = type;
		this.tauxPlacement = tauxPlacement;
		this.setTransList(new ArrayList<>());
		
	}
	
	@Override
	public String toString() {
		return "Account [numCompt=" + numCompt + ", type=" + type + ", tauxPlacement=" + tauxPlacement
				+ "]";
	}

	public String getNumCompt() {
		return numCompt;
	}

	public String getType() {
		return type;
	}

	public double getTauxPlacement() {
		return tauxPlacement;
	}
	public void setTauxPlacement(double tauxPlacement) {
		this.tauxPlacement = tauxPlacement;
	}

	public ArrayList<Transaction> getTransList() {
		return transList;
	}

	public void setTransList(ArrayList<Transaction> transList) {
		this.transList = transList;
	}
}
