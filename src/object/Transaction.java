package object;

import java.text.DateFormat;
import java.util.Date;

public class Transaction {
	
	private String theme;
	private double montant;
	private String moyen;
	private DateFormat shortDateFormat;
	
	public Transaction(String moyen, double montant, String theme) {
		DateFormat shortDateFormat = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM);
		this.montant = montant;
		this.moyen = moyen;
		this.theme = theme;
	}


	@Override
	public String toString() {
		return "Transaction [theme=" + theme + ", montant=" + montant + ", moyen=" + moyen + ", Date="
				+ shortDateFormat + "]";
	}
	public String getTheme() {
		return theme;
	}
	
	public void setTheme(String theme) {
		this.theme = theme;
	}

	public double getMontant() {
		return montant;
	}

	public void setMontant(double montant) {
		this.montant = montant;
	}

	public String getMoyen() {
		return moyen;
	}

	public void setMoyen(String moyen) {
		this.moyen = moyen;
	}

	public DateFormat getDate() {
		return shortDateFormat;
	}

	public void setDate(DateFormat shortDateFormat) {
		this.shortDateFormat = shortDateFormat;
	}


}
