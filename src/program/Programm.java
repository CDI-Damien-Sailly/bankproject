package program;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import command.CommandCreateAccount;
import command.CommandExit;
import command.CommandPrintAccount;
import command.CommandPrintBank;
import command.CommandWrong;
import interf.ICommand;

public class Programm {
	
	private static final Map<String, String> COMMANDES_LIST_DESC = new HashMap();
	
	public static void run() {
		for(final Entry<String,String>e : COMMANDES_LIST_DESC.entrySet()) {
			System.out.println(e.getKey()+ " : "+e.getValue());
		}
	}
	
	
	public static void addCommandeDesc(String cmd, String desc) {
		COMMANDES_LIST_DESC.put(cmd, desc);
		CommandCreateAccount.chargerStaticPortion();
		CommandExit.chargerStaticPortion();
		CommandPrintAccount.chargerStaticPortion();
		CommandPrintBank.chargerStaticPortion();
		
	}
	
	public static ICommand create(String cmd) {
		ICommand theCommand= null;
		if(CommandCreateAccount.CMD.equals(cmd)) {
			theCommand = new CommandCreateAccount();
		}else if(CommandPrintAccount.CMD.equals(cmd)) {
			theCommand = new CommandPrintAccount();
		}else if(CommandPrintBank.CMD.equals(cmd)) {
			theCommand = new CommandPrintBank();
		}else if(CommandExit.CMD.equals(cmd)) {
			theCommand = new CommandExit();
		}else {
			theCommand = new CommandWrong();
		}
		
		return theCommand;
	}

	public static Map<String, String> getCommandesListDesc() {
		return COMMANDES_LIST_DESC;
	}

}
