package tools;

import java.util.Scanner;

public class Clavier {
	
	private static Scanner sc = new Scanner (System.in);
	
	private Clavier() {
		
	}
	
	public static String readLine(){
		return sc.nextLine();
	}
	public static int readInt(){
		return sc.nextInt();
	}
	public static double readDouble(){
		return sc.nextDouble();
	}
	

}
